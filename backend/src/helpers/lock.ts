import { EventEmitter } from "events";

export default class Lock {
    private locked: boolean;
    private dispatcher: EventEmitter;

    constructor() {
        this.locked = false;
        this.dispatcher = new EventEmitter();
    }

    public acquire() {
        return new Promise(resolve => {
            // If nobody has the lock, take it and resolve immediately
            if (!this.locked) {
                // Safe because JS doesn't interrupt you on synchronous operations,
                // so no need for compare-and-swap or anything like that.
                this.locked = true;
                return resolve();
            }

            const listener = this.tryToGetLock(resolve);

            this.dispatcher.on("release", listener);
        });
    }

    public release() {
        // Release the lock immediately
        this.locked = false;
        setImmediate(() => this.dispatcher.emit("release"));
    }

    // Otherwise, wait until somebody releases the lock and try again
    private tryToGetLock = (resolve: any) => {
        return () => {
            if (!this.locked) {
                this.locked = true;
                this.dispatcher.removeListener("release", this.tryToGetLock);
                return resolve();
            }
        };
    };
}
