import { Express, Router } from "express";
import express = require("express");
import Logger from "./logger";
import MessagesRoute from "./api/messages/routes";

import * as cors from "cors";
import bodyParser = require("body-parser");

export default class Application {
    private static logger: Logger  = new Logger("Application");

    private port: number;
    private server: Express;
    private router: Router;

    constructor(port: number) {
        this.port = port;
        this.server = express();
        this.router = express.Router();
    }

    public initialize = () => {
        Application.logger.log(`Initializing...`);

        this.server.use(cors({
            origin: "*"
        }));

        this.server.use(bodyParser.json());

        MessagesRoute.init(this.router);

        this.server.use("/", this.router);

        Application.logger.log(`Listening on port: ${this.port}`);
        this.server.listen(this.port);

        Application.logger.log(`Initialized!`);
        return this;
    }
}

new Application(8080).initialize();
