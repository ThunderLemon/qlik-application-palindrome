
import uuidv4 = require("uuid/v4");
import Lock from "../../helpers/lock";

type MessagesByUsers = {
    user: string;
    messages: Map<string, string>;
};

export default class Messages {

    public static getInstance(): Messages {
        if (!Messages.instance) {
            Messages.instance = new Messages();
        }
        return Messages.instance;
    }

    private static instance: Messages;

    private messagesByUsers: MessagesByUsers[];
    private lock = new Lock();

    private constructor() {
        // Do nothing
        this.messagesByUsers = [];
    }

    public async add(user: string, message: string): Promise<string> {
        await this.lock.acquire();

        const id = uuidv4();
        for (const saved of this.messagesByUsers) {
            if (saved.user !== user) {
                continue;
            }

            // Existing user
            saved.messages.set(id, message);
            this.lock.release();
            return id;
        }

        // New user
        const messages = new Map<string, string>();
        messages.set(id, message);
        this.messagesByUsers.push({user, messages});

        this.lock.release();
        return id;
    }

    public async remove(user: string, id: string): Promise<boolean> {
        await this.lock.acquire();

        for (const saved of this.messagesByUsers) {
            if (saved.user !== user) {
                continue;
            }

            this.lock.release();
            return saved.messages.delete(id);
        }

        this.lock.release();
        return false;
    }

    public async get(user: string) {
        await this.lock.acquire();

        for (const saved of this.messagesByUsers) {
            if (saved.user !== user) {
                continue;
            }

            this.lock.release();
            return saved.messages;
        }
        this.lock.release();
    }

    public isPalindrome(check: string): boolean {
        // Here we take remove any non-word characterwith \W which is equal to [^a-zA-Z0-9_]
        // This makes it possible to check palindromes such as:
        // Are we not pure? “No, sir!” Panama’s moody Noriega brags. “It is garbage!” Irony dooms a man--a prisoner up to new era
        // Yo, Banana boy!
        const clean = check.toLowerCase().replace(/\W/g, "");
        let c1 = 0;
        let c2 = clean.length - 1;
        while (c1 < c2) {
            if (clean.charAt(c1) !== clean.charAt(c2)) {
                return false;
            }
            c1++;
            c2--;
        }
        return true;
    }
}
