import * as React from 'react';
import * as ReactDOM from 'react-dom';
import App from './components/App';

import "materialize-css/dist/css/materialize.min.css";
import "materialize-css/dist/js/materialize.js";

import './styles.css';

ReactDOM.render(
  <App />,
  document.getElementById('root') as HTMLElement
);
