import * as React from "react";
import Delete from '@material-ui/icons/Delete';
import KeyboardArrowDownSharp from '@material-ui/icons/KeyboardArrowDownSharp';
import KeyboardArrowUpSharp from '@material-ui/icons/KeyboardArrowUpSharp';
import axios from 'axios';
import { MESSAGES_API_ROUTE } from 'src/constants';

type MessageProps = {
    id: string;
    text: string;
    user: string;
    onDelete: () => void;
};

type MessageState = {
    extended: boolean;
    isPalindrome?: boolean;
};

export class Message extends React.Component<MessageProps, MessageState, {}> {

    public state = {
        extended: false,
        isPalindrome: undefined
    };

    public render() {
        return (
            <div className="message-container card blue-grey lighten-1" onClick={this.toggleExtended}>
                <div className="card-content">
                    <div className="text-container">
                        { this.state.extended ? <KeyboardArrowUpSharp /> : <KeyboardArrowDownSharp />}
                        { this.props.text }
                    </div>
                    { this.renderExtendedDetails() }
                    <a className="waves-effect waves-light btn-floating red icon lighten-2 delete" onClick={this.deleteMessage}><Delete/></a>
                </div>
            </div>
        );
    }

    private renderExtendedDetails(): React.ReactNode | undefined {
        if (!this.state.extended) {
            return undefined;
        }

        if (this.state.isPalindrome === undefined) {
            return undefined;
        }

        let content = "This is just a regular message...";
        if (this.state.isPalindrome) {
            content = "Wow, This message is a palindrome, who would have thought so!";
        }

        return (<div className="details">{content}<br/>Identifer: {this.props.id}</div>);
    }

    private toggleExtended = async () => {
        const state: MessageState = {
            extended: !this.state.extended
        };

        const response = await axios.get(`${MESSAGES_API_ROUTE}/${this.props.user}/${this.props.id}?palindrome=true`);
        if (response.status === 200 && this.state.isPalindrome === undefined) {
            state.isPalindrome = response.data;
        }

        this.setState(state);
    }

    private deleteMessage = async () => {
        const response = await axios.delete(`${MESSAGES_API_ROUTE}/${this.props.user}/${this.props.id}`);
        if (response.status === 200) {
            this.props.onDelete();
        }
    }
}
