import { Router, Request, Response } from "express";
import Logger from "../../logger";
import { validationResult, checkSchema} from "express-validator/check";
import { schemas } from "./schemas";
import Messages from "./messages";

const route: string = "/messages";

export default class MessagesRoute {
    public static logger: Logger = new Logger("Messages");
    public static init = (router: Router) => {
        router.get(`${route}/:user`, checkSchema(schemas.getAll), async (req: Request, res: Response) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            MessagesRoute.logger.log("Getting all messages for user", req.params);
            const messages = await Messages.getInstance().get(req.params.user);
            if (!messages) {
                res.status(200);
                res.send([]);
                return;
            }

            res.status(200);
            res.send(JSON.stringify([...messages]));
        });

        router.get(`${route}/:user/:id`, checkSchema(schemas.get), async (req: Request, res: Response) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            const service = Messages.getInstance();
            const messages = await service.get(req.params.user);
            if (!messages) {
                res.status(204).send();
                return;
            }

            MessagesRoute.logger.log("Getting one message", req.params);

            const message = messages.get(req.params.id);
            if (!message) {
                res.status(204).send();
                return;
            }

            if (req.query.palindrome) {
                res.status(200);
                res.send(service.isPalindrome(message));
                return;
            }

            res.status(200);
            res.send(message);
        });

        router.post(`${route}/:user`, checkSchema(schemas.create), async (req: Request, res: Response) => {
            console.log(req.body);
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            MessagesRoute.logger.log("Saving new user message", req.params);
            const message = req.query.message || req.body.message;
            const id = await Messages.getInstance().add(req.params.user, message);

            res.status(200);
            res.send(JSON.stringify({"message" : message, id}));
        });

        router.delete(`${route}/:user/:id`, checkSchema(schemas.delete), async (req: Request, res: Response) => {
            const errors = validationResult(req);
            if (!errors.isEmpty()) {
                return res.status(400).json({ errors: errors.array() });
            }

            MessagesRoute.logger.log("Deleting user message", req.params);
            const result = await Messages.getInstance().remove(req.params.user, req.params.id);

            if (result) {
                res.status(200);
                res.send("OK");
            } else {
                res.status(204);
            }
        });
    }
}
