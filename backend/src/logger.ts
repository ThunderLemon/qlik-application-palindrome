
type LoggingType = "warn" | "error" | "log";

export default class Logger {
    private name: string;

    constructor(name: string) {
        this.name = name;
    }

    public warn = (message: string, data?: any[]): void => {
        this.logging("warn", message, data);
    }

    public error = (message: string, data?: any[]): void => {
        this.logging("error", message, data);
    }

    public log = (message: string, data?: any[]): void => {
        this.logging("log", message, data);
    }

    private logging = (type: LoggingType, message: string, data?: any[]) => {
        if (data) {
            console[type](`${this.name}: ${message}`, data);
            return;
        }

        console[type](`${this.name}: ${message}`);
    }
}
