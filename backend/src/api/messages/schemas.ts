import { ValidationParamSchema } from "express-validator/check";

const user: ValidationParamSchema = {
    in: ["params"],
    isString: { errorMessage: "This field needs to be a string" },
    isEmpty: { negated : true, errorMessage: "This field cannot be empty" }
};

const message: ValidationParamSchema = {
    in: ["body"],
    isString: {errorMessage: "This field needs to be a string"},
    isEmpty: { negated : true, errorMessage: "This field cannot be empty"}
};

const id: ValidationParamSchema = {
    in: ["params"],
    isString: { errorMessage: "This field needs to be a string" },
    isEmpty: { negated : true, errorMessage: "This field cannot be empty" }
};

const palindrom: ValidationParamSchema = {
    in: ["query"],
    optional: true,
    isBoolean: { errorMessage: "This field needs to be a boolean" }
};

export const schemas  = {
    get: { user, id, palindrom },
    getAll : { user },
    create : { user, message },
    delete : { user, id }
};
