import * as React from "react";
import Refresh from '@material-ui/icons/Refresh';
import axios from 'axios';

import { Message } from './Message';

export const SERVER_API_URL = "http://localhost:8080";
export const MESSAGES_API_ROUTE = `${SERVER_API_URL}/messages`;

type AppState = {
    user: string;
    message: string;
    messages: Map<string, string>;
};

class App extends React.Component<{}, AppState, {}> {

    public state = {
        user: "",
        message: "",
        messages: new Map<string, string>(),
    };

    public render() {
        return (
            <div className="container">
                <div className="card blue-grey darken-3">
                    <div className="card-content white-text">
                        <span className="card-title">Who are you?</span>
                        <div className="input-field col s12">
                            <input placeholder="Enter your full name" id="full_name" type="text" className="validate" onChange={this.handleNameChange} value={this.state.user}/>
                            <label>Full name</label>
                        </div>
                    </div>
                </div>
                <div className="card blue-grey darken-3">
                    <div className="card-content white-text">
                        <span className="card-title">Send a message here!</span>
                        <div className="row">
                            <div className="input-field col s12">
                                <input placeholder="" id="message" type="text" className="validate" onChange={this.handleMessageChange} value={this.state.message}/>
                                <label>Message</label>
                            </div>
                        </div>
                    </div>
                    <div className="card-action">
                        <a className="waves-effect waves-light btn green"  onClick={this.sendMessage}>Send</a>
                    </div>
                </div>
                <div className="card blue-grey darken-3">
                    <div className="card-content white-text">
                        <a className="waves-effect waves-light btn-floating green icon refresh" onClick={this.getMessageHistory}><Refresh/></a>
                        <span className="card-title card-title__history">Messages History</span>
                        <div className="row message-history-container">
                            {this.renderMessageHistory()}
                        </div>
                    </div>
                </div>
            </div>
        );
    }

    private renderMessageHistory = () => {
        const messages = [];
        if (this.state.messages.size === 0) {
            return (
                <div className="no-messages">
                    There is no messages yet, try pressing refreah or sending a message.
                </div>
            );
        }

        for (const message of this.state.messages) {
            messages.push((<Message id={message[0]} text={message[1]} user={this.state.user} onDelete={this.getMessageHistory} />));
        }

        return messages;
    }

    private handleMessageChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({message: event.target.value});
    }

    private handleNameChange = (event: React.ChangeEvent<HTMLInputElement>) => {
        this.setState({user: event.target.value});
    }

    private sendMessage = async () => {
        const response = await axios.post(`${MESSAGES_API_ROUTE}/${this.state.user}`, { message : this.state.message });
        if (response.status !== 200) {
            return;
        }

        this.setState({message: ""}, this.getMessageHistory);
    }

    private getMessageHistory = async () => {
        const response = await axios.get<Array<[string, string]>>(`${MESSAGES_API_ROUTE}/${this.state.user}`);
        if (response.status !== 200) {
            return;
        }

        const messages = new Map<string, string>();
        if (response.data.length) {
            for (const data of response.data) {
                messages.set(data[0], data[1]);
            }
        }
        this.setState({messages});
    };

}

export default App;
